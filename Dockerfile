ARG CI_REGISTRY
FROM debian:stretch-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjdk-11-jre && \
    sed -i -e "s/keystore\.type=pkcs12/keystore.type=jks/g" /etc/java-11-openjdk/security/java.security && \
    rm /etc/ssl/certs/java/cacerts && \
    /var/lib/dpkg/info/ca-certificates-java.postinst configure
